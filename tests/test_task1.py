import subprocess
from pathlib import Path


def test_unknown_txt_file_unmodified():
    """
    检查 unknown.txt 文件未被修改
    """
    s = Path("unknown.txt").read_bytes()
    assert s == (
        b"v\xdb^tN\r\x91\xcdge\xff\x0cN\x00e\xe5\x96\xbeQ\x8dfh0\x02\x00\n"
        b"S\xcae\xf6_SR\xc9R\xb1\xff\x0c\\\x81g\x08N\r_\x85N\xba0\x02\x00\n"
        b"&\x15\xfe\x0f\x00\n"
    )


def test_convert_py_script():
    """
    检查 convert.py 脚本的运行效果是否符合要求
    """
    f = Path("known.txt")
    # 删除旧有的 known.txt 文件
    f.unlink(missing_ok=True)
    # 运行脚本
    subprocess.run(["/usr/bin/env", "python", "convert.py"], check=True)
    # 检查 known.txt 存在
    assert f.is_file()
    b = f.read_bytes()
    # 检查前 4 个字节符合要求
    assert b[:4] == b"\xe7\x9b\x9b\xe5"
    # 检查后 4 个字节 (不计最后的换行符) 符合要求
    assert b[-6:-2] == b"\x95\xef\xb8\x8f"
    # 检查换行符的数量符合要求
    assert b.count(b"\r\n") == 3
    # 检查第一个换行符的位置符合要求
    assert b.index(b"\r\n") == 36
    # 删除 known.txt 文件, 以免被提交
    f.unlink()
