from pathlib import Path


def test_data_unix_csv_file_exists():
    """
    检查 data_unix.csv 文件已被创建
    """
    f = Path("data_unix.csv")
    assert f.exists()


def test_data_unix_csv_file_content():
    """
    检查 data_unix.csv 文件的内容符合要求
    """
    f = Path("data_unix.csv")
    b = f.read_bytes()
    assert b == (
        b"\xe5\x9b\xbd\xe5\xae\xb6,\xe9\xa6\x96\xe9\x83\xbd\n"
        b"\xe4\xb8\xad\xe5\x9b\xbd,\xe5\x8c\x97\xe4\xba\xac\n"
        b"\xe7\xbe\x8e\xe5\x9b\xbd,\xe5\x8d\x8e\xe7\x9b\x9b\xe9\xa1\xbf\n"
        b"\xe4\xbf\x84\xe7\xbd\x97\xe6\x96\xaf,"
        b"\xe8\x8e\xab\xe6\x96\xaf\xe7\xa7\x91\n"
    )
