import subprocess
from pathlib import Path


def test_make_excel_csv_py_script():
    """
    检查 convert.py 脚本的运行效果是否符合要求
    """
    f = Path("data_excel.csv")
    # 删除旧有的 data_excel.csv 文件
    f.unlink(missing_ok=True)
    # 运行脚本
    subprocess.run(["/usr/bin/env", "python", "make_excel_csv.py"], check=True)
    # 检查 data_excel.csv 存在
    assert f.is_file()
    b = f.read_bytes()
    # 检查前 4 个字节符合要求
    assert b[:4] == b"\xef\xbb\xbf\xe5"
    # 检查后 4 个字节 (不计最后的换行符) 符合要求
    assert b[-6:-2] == b"\xaf\xe7\xa7\x91"
    # 检查换行符的数量符合要求
    assert b.count(b"\r\n") == 4
    # 检查第一个换行符的位置符合要求
    assert b.index(b"\r\n") == 16
