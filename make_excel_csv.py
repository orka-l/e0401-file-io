import pandas as pd
df = pd.DataFrame(
    {
        "国家": ["中国", "美国", "俄罗斯"],
        "首都": ["北京", "华盛顿", "莫斯科"],
    }
)
df.to_csv('data_excel.csv',index = False,encoding = 'utf_8_sig',lineterminator = '\r\n')
